package br.com.clinica.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by danieldantas on 09/07/17.
 */
@Entity
@Data
@AllArgsConstructor
public class Paciente implements Serializable{
	
	private static final long serialVersionUID = 8800083290111255642L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private String nome;
    private String documento;
    private String responsavel;
    @Type(type="date")
    @DateTimeFormat (pattern="yyyy-MM-dd")
    private Date dataNascimento;
    private String telefoneCelular;
    private String telefoneFixo;
    private String endereco;
    private String email;
    private String observacao;
    @Type(type="date")
    @DateTimeFormat (pattern="yyyy-MM-dd")
    private Date dataInicio;
    @Type(type="date")
    @DateTimeFormat (pattern="yyyy-MM-dd")
    private Date dataUltimaConsulta;
    private Integer tipoRelacionamento;
    //private List<PacienteDocumento> documentoList;

    public Paciente(){}

    public Paciente(String nome, String documento) {
        this.nome = nome;
        this.documento = documento;
    }
}
