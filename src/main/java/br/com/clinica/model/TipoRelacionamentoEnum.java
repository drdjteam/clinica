package br.com.clinica.model;

/**
 * Created by danieldantas on 09/07/17.
 */
public enum TipoRelacionamentoEnum {
	CONVENIO(0),
	PARTICULAR(1);
	
    private int tipoRelacionamentoId;

    TipoRelacionamentoEnum(int id) {
        this.tipoRelacionamentoId = id;
    }

    public int getId() {
        return tipoRelacionamentoId;
    }
}
