package br.com.clinica.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor
public class Curso implements Serializable{
	private static final long serialVersionUID = -5545266294920851741L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String titulo;
    
//    @OneToMany(mappedBy = "curso", cascade = CascadeType.ALL)
    
    private Integer publicoAlvo;
    
    private Integer cargaHoraria;
    private String area;
    private Integer limiteParticipantes;
    private Integer cursoCategoria;
    private String preRequisto;
    @Type(type="date")
    @DateTimeFormat (pattern="yyyy-MM-dd")  
    private Date dataInicio;
    @Type(type="date")
    @DateTimeFormat (pattern="yyyy-MM-dd")  
    private Date dataTermino;
    
    
    public Curso(){}
    
}
