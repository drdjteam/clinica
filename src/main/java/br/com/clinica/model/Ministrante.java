package br.com.clinica.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor
public class Ministrante implements Serializable{

	private static final long serialVersionUID = 4855368765566725588L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private String area;
    private String rg;
    private String cpf;
    @Type(type="date")
    @DateTimeFormat (pattern="yyyy-MM-dd")    
    private Date dataNacimento;
    private String telefoneCelular;
    private String telefoneFixo;
    private String endereco;
    private String email;
    private String observacao;    

    public Ministrante(){}
}
