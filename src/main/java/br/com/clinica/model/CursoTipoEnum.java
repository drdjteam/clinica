package br.com.clinica.model;

public enum CursoTipoEnum {
	TEORICO(0,"Teórico"),
	PRATICO(1,"Prático");
	
    private int id;
    private String descricao;

    CursoTipoEnum(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public int getId() {
        return this.id;
    }
    
    public String getDescricao() {
    		return this.descricao;
    }
}
