package br.com.clinica.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by danieldantas on 09/07/17.
 */
@Entity
@Data
@AllArgsConstructor
public class Aluno implements Serializable{

	private static final long serialVersionUID = -859694658131923668L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private String nacionalidade;
    private String estadoCivil;
    private String rg;
    private String rgOrgaoEmissorUF;
//    @CPF(message="CPF inválido")
    private String cpf;
    @Type(type="date")
    @DateTimeFormat (pattern="yyyy-MM-dd")    
    private Date dataNacimento;
    private String profissao;
    private String endereco;
    private Integer numero;
    private String complemento;
    private String bairro;
    private String cidade;
    private String estado;
    private String cep;
    private String telefoneResidencial;
    private String telefoneComercial;
    private String telefoneCelular;
    private String email;
    private String contatoEmergenciaNome;
    private String contatoEmergenciaTelefone;
    private String comoSoubeCurso;
    private boolean exAluno;
    private String exAlunoNomeCurso;
    private Integer exAlunoAnoRealizacao;
    @Type(type="date")
    @DateTimeFormat (pattern="yyyy-MM-dd")        
    private Date dataRealizacaoUltimoCurso;
    @Type(type="date")
    @DateTimeFormat (pattern="yyyy-MM-dd")    
    private Date dataInscricao;
    private String observacaoPagamento;
    private String observacaoPagamentoExAluno;
    private String fileNameImport;
//    private Byte[] List<AlunoDocumento>; //diploma , ceriticados

    public Aluno(){}
}
