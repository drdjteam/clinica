package br.com.clinica.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by danieldantas on 09/07/17.
 */
@Entity
@Data
@AllArgsConstructor
public class Profissional implements Serializable{

	private static final long serialVersionUID = -7337950023315230919L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private String rg;
    private String cpf;
    @Type(type="date")
    @DateTimeFormat (pattern="yyyy-MM-dd")    
    private Date dataNacimento;
    private String telefoneCelular;
    private String telefoneFixo;
    private String endereco;
    private String email;
    private String disponibilidade;
    private String funcao;
    private String registroProfissional;
    private String observacao;
    private Byte[] contrato;
    private Byte[] diploma;

    public Profissional(){}
}
