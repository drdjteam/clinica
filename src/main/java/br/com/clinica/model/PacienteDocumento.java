package br.com.clinica.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import lombok.Data;

/**
 * Created by danieldantas on 09/07/17.
 */
@Entity
@Data
public class PacienteDocumento {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idPacienteDocumento;
    @Lob
    private byte[] documento;

    protected PacienteDocumento(){}
}
