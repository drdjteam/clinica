package br.com.clinica.model;

public enum PublicoAlvoEnum {
	FISIO(0,"Fisio"),
	FONO(1,"Fono"),
	PSICO(2,"Psico"),
	MEDICO(3,"Médico"),
	PEDAGOGO(4,"Pedagogo"),
	PSICOPEDAGOGO(5,"Psicopedagogo");
	
    private int id;
    private String descricao;

    PublicoAlvoEnum(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public int getId() {
        return this.id;
    }
    
    public String getDescricao() {
    		return this.descricao;
    }
}
