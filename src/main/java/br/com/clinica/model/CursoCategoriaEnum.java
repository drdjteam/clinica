package br.com.clinica.model;

public enum CursoCategoriaEnum {
	PRESENCIAL(0,"Presencial"),
	EAD(1,"Ead");
	
    private int id;
    private String descricao;

    CursoCategoriaEnum(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public int getId() {
        return this.id;
    }
    
    public String getDescricao() {
    		return this.descricao;
    }
}
