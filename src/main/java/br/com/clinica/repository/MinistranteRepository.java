package br.com.clinica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.clinica.model.Ministrante;

public interface MinistranteRepository extends JpaRepository<Ministrante, Long> {

        List<Ministrante> findByNome(String nome);

}
