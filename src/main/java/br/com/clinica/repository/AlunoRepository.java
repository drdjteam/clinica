package br.com.clinica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.clinica.model.Aluno;

public interface AlunoRepository extends JpaRepository<Aluno, Long> {

        List<Aluno> findByNome(String nome);

}
