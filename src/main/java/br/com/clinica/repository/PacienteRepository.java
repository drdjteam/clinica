package br.com.clinica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.clinica.model.Paciente;

/**
 * Created by danieldantas on 09/07/17.
 */
public interface PacienteRepository extends JpaRepository<Paciente, Long> {

    List<Paciente> findByNome(String nome);

}
