package br.com.clinica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.clinica.model.Profissional;

/**
 * Created by danieldantas on 09/07/17.
 */
public interface ProfissionalRepository extends JpaRepository<Profissional, Long> {

        List<Profissional> findByNome(String nome);

}
