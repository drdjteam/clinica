package br.com.clinica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.clinica.model.Curso;

public interface CursoRepository extends JpaRepository<Curso, Long> {

        List<Curso> findByTitulo(String titulo);

}
