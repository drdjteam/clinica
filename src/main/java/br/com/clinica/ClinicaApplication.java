package br.com.clinica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"br.com.clinica","br.com.curso"})// same as @Configuration @EnableAutoConfiguration @ComponentScan
public class ClinicaApplication {

//	private static final Logger log = LoggerFactory.getLogger(ClinicaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ClinicaApplication.class, args);
	}

/*	
	@Bean
	public CommandLineRunner demo(PacienteRepository repository) {
		return (args) -> {
			// save a couple of pacientes
			repository.save(new Paciente("Jack", "8654654654"));
			repository.save(new Paciente("Chloe", "6546546546654"));
			repository.save(new Paciente("Kim", "654321564"));
			repository.save(new Paciente("David", "654654654654"));
			repository.save(new Paciente("Michelle", "65465464564"));
			repository.save(new Paciente("Jose", "12335464564"));

			// fetch all pacientes
			log.info("pacientes found with findAll():");
			log.info("-------------------------------");
			for (Paciente paciente : repository.findAll()) {
				log.info(paciente.toString());
			}
			log.info("");

			// fetch an individual paciente by ID
			Paciente paciente = repository.findOne(1L);
			log.info("paciente found with findOne(1L):");
			log.info("--------------------------------");
			log.info(paciente.toString());
			log.info("");

			// fetch pacientes by last name
			log.info("paciente found with findByName('Kim'):");
			log.info("--------------------------------------------");
			for (Paciente paciente1 : repository.findByNome("Kim")) {
				log.info(paciente1.toString());
			}
			log.info("");
		};
	}
*/


}
