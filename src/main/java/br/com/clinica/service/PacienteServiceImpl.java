package br.com.clinica.service;

import br.com.clinica.model.Paciente;
import br.com.clinica.repository.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by danieldantas on 12/07/17.
 */
@Service("pacienteService")
@Transactional
public class PacienteServiceImpl implements PacienteService{

    @Autowired
    private PacienteRepository repository;

    @Override
    public Paciente findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public List<Paciente> findByNome(String nome) {
        return repository.findByNome(nome);
    }

    @Override
    public void savePaciente(Paciente paciente) {
        repository.save(paciente);
    }

    @Override
    public void updatePaciente(Paciente paciente) {
        savePaciente(paciente);
    }

    @Override
    public void deletePacienteById(Long id) {
        repository.delete(id);
    }

    @Override
    public List<Paciente> findAllPacientes() {
        return repository.findAll();
    }

    @Override
    public boolean isPacienteExist(Paciente paciente) {
        boolean ret = false;
        List<Paciente> pacienteList = repository.findByNome(paciente.getNome());
        if (pacienteList != null) {
            if (pacienteList.size() > 0) {
                ret = true;
            }
        }

        return ret;
    }
}
