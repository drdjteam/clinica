package br.com.clinica.service;

import br.com.clinica.model.Profissional;

import java.util.List;

/**
 * Created by danieldantas on 12/07/17.
 */
public interface ProfissionalService {
    public Profissional findById(Long id);

    public List<Profissional> findByNome(String nome);

    public void saveProfissional(Profissional Profissional);

    public void updateProfissional(Profissional Profissional);

    public void deleteProfissionalById(Long id);

    public List<Profissional> findAllProfissionals();

    public boolean isProfissionalExist(Profissional Profissional);
}
