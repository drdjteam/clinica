package br.com.clinica.service;

import br.com.clinica.model.Paciente;

import java.util.List;

/**
 * Created by danieldantas on 12/07/17.
 */
public interface PacienteService {
    public Paciente findById(Long id);

    public List<Paciente> findByNome(String nome);

    public void savePaciente(Paciente Paciente);

    public void updatePaciente(Paciente Paciente);

    public void deletePacienteById(Long id);

    public List<Paciente> findAllPacientes();

    public boolean isPacienteExist(Paciente Paciente);
}
