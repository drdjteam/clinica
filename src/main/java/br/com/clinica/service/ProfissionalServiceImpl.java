package br.com.clinica.service;

import br.com.clinica.model.Profissional;
import br.com.clinica.model.Profissional;
import br.com.clinica.repository.ProfissionalRepository;
import br.com.clinica.repository.ProfissionalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by danieldantas on 12/07/17.
 */
@Service("profissionalService")
@Transactional
public class ProfissionalServiceImpl implements ProfissionalService{

    @Autowired
    private ProfissionalRepository repository;

    @Override
    public Profissional findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public List<Profissional> findByNome(String nome) {
        return repository.findByNome(nome);
    }

    @Override
    public void saveProfissional(Profissional profissional) {
        repository.save(profissional);
    }

    @Override
    public void updateProfissional(Profissional profissional) {
        saveProfissional(profissional);
    }

    @Override
    public void deleteProfissionalById(Long id) {
        repository.delete(id);
    }

    @Override
    public List<Profissional> findAllProfissionals() {
        return repository.findAll();
    }

    @Override
    public boolean isProfissionalExist(Profissional profissional) {
        return repository.findByNome(profissional.getNome()) != null;
    }
}
