package br.com.clinica.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.clinica.model.Aluno;
import br.com.clinica.repository.AlunoRepository;
import br.com.curso.service.AlunoService;


@Controller
public class AlunoController {
	
	@Autowired
	private AlunoRepository alunos;
	
    @Autowired
    AlunoService alunoService; //Service which will do all data retrieval/manipulation work
	
	@RequestMapping("/alunos")
	public ModelAndView listar() {
		
		//List<Aluno> todosAlunos = alunos.findAll();
		List<br.com.clinica.model.Aluno> todosAlunos = (List<Aluno>) alunos.findAll();
		
		ModelAndView mv = new ModelAndView("alunoListar");
		
		mv.addObject("alunos", todosAlunos);
		
		mv.addObject(new Aluno());
		
		return mv;
	}
	
	@RequestMapping(value = "/alunos", method = RequestMethod.POST)
	public String salvar(@Validated Aluno aluno, Errors errors) {
		
		if ( errors.hasErrors() ) {
			return "redirect:/alunos";
		}
		
		this.alunos.save(aluno);
		
		return "redirect:/alunos";
	}
	
	@RequestMapping("/excluialuno/{id}")
	public String excluir(@PathVariable Long id) {
		
		this.alunos.delete(id);
		
		return "redirect:/alunos";
	}
	
	@RequestMapping("/editaaluno/{aluno}")
	public ModelAndView editar(@PathVariable Aluno aluno) {
		
		ModelAndView mv = new ModelAndView("alunoEditar");
		
		mv.addObject(aluno);
		
		return mv;
	}
	
	@RequestMapping("/alteraaluno")
	public String editaralterar(@Validated Aluno aluno) {
		
		Aluno alunonovo = aluno;

		this.alunos.delete(aluno.getId());

		this.alunos.save(alunonovo);

		return "redirect:/alunos";
	}

	
	
}
