package br.com.clinica.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class MainController {
	
	@RequestMapping("/home")
	public ModelAndView mostrar() {
		
		ModelAndView mv = new ModelAndView("home");
		
		return mv;
	}	

    @GetMapping("/")
    public String home1() {
        return "/home";
    }

    @GetMapping("")
    public String login2() {
        return "/login";
    }
    
    @GetMapping("/login")
    public String login() {
        return "/login";
    }
    
    @GetMapping("/403")
    public String error403() {
        return "/error/403";
    }
    
    @PostMapping("/")
    public String alunoUpload(@RequestParam("file") MultipartFile file,
            RedirectAttributes redirectAttributes) {

        //storageService.store(file);
    	System.out.println("sdsdf"); 
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");

        return "redirect:/";
    }
}
