package br.com.clinica.controller;

import br.com.clinica.model.Profissional;
import br.com.clinica.service.ProfissionalService;
import br.com.clinica.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by danieldantas on 13/07/17.
 */
@RestController
@RequestMapping("/api")
public class ProfissionalControllerRestApi {
    public static final Logger logger = LoggerFactory.getLogger(ProfissionalControllerRestApi.class);

    @Autowired
    ProfissionalService profissionalService; //Service which will do all data retrieval/manipulation work

    // -------------------Retrieve All Profissionals---------------------------------------------

    @SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/profissional/", method = RequestMethod.GET)
    public ResponseEntity<List<Profissional>> listAllProfissionals() {
        List<Profissional> profissionals = profissionalService.findAllProfissionals();
        if (profissionals.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Profissional>>(profissionals, HttpStatus.OK);
    }

    // -------------------Retrieve Single Profissional------------------------------------------

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/profissional/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProfissional(@PathVariable("id") long id) {
        logger.info("Fetching Profissional with id {}", id);
        Profissional profissional = profissionalService.findById(id);
        if (profissional == null) {
            logger.error("Profissional with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType("Profissional with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Profissional>(profissional, HttpStatus.OK);
    }

    // -------------------Create a Profissional-------------------------------------------
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/profissional/", method = RequestMethod.POST)
    public ResponseEntity<?> createProfissional(@RequestBody Profissional profissional, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Profissional : {}", profissional);

        if (profissionalService.isProfissionalExist(profissional)) {
            logger.error("Unable to create. A Profissional with name {} already exist", profissional.getNome());
            return new ResponseEntity(new CustomErrorType("Unable to create. A Profissional with name " +
                    profissional.getNome() + " already exist."),HttpStatus.CONFLICT);
        }
        profissionalService.saveProfissional(profissional);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/profissional/{id}").buildAndExpand(profissional.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    // ------------------- Update a Profissional ------------------------------------------------
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/profissional/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProfissional(@PathVariable("id") long id, @RequestBody Profissional profissional) {
        logger.info("Updating Profissional with id {}", id);

        Profissional currentProfissional = profissionalService.findById(id);

        if (currentProfissional == null) {
            logger.error("Unable to update. Profissional with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType("Unable to upate. Profissional with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        currentProfissional.setNome(profissional.getNome());
        currentProfissional.setEmail(profissional.getEmail());

        profissionalService.updateProfissional(currentProfissional);
        return new ResponseEntity<Profissional>(currentProfissional, HttpStatus.OK);
    }

    // ------------------- Delete a Profissional-----------------------------------------
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/profissional/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProfissional(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting Profissional with id {}", id);

        Profissional profissional = profissionalService.findById(id);
        if (profissional == null) {
            logger.error("Unable to delete. Profissional with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType("Unable to delete. Profissional with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        profissionalService.deleteProfissionalById(id);
        return new ResponseEntity<Profissional>(HttpStatus.NO_CONTENT);
    }


}
