package br.com.clinica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.clinica.model.Paciente;
import br.com.clinica.repository.PacienteRepository;

@Controller
public class PacienteController {
	
	@Autowired
	private PacienteRepository pacientes;
	
	@RequestMapping("/pacientes")
	public ModelAndView listar() {
		
		//List<Paciente> todosPacientes = pacientes.findAll();
		List<Paciente> todosPacientes = (List<Paciente>) pacientes.findAll();
		
		ModelAndView mv = new ModelAndView("pacienteListar");
		
		mv.addObject("pacientes", todosPacientes);
		
		mv.addObject(new Paciente());
		
		return mv;
	}
	
	@RequestMapping(value = "/pacientes", method = RequestMethod.POST)
	public String salvar(@Validated Paciente paciente, Errors errors) {
		
		if ( errors.hasErrors() ) {
			return "redirect:/pacientes";
		}
		
		this.pacientes.save(paciente);
		
		return "redirect:/pacientes";
	}
	
	@RequestMapping("/excluipaciente/{id}")
	public String excluir(@PathVariable Long id) {
		
		this.pacientes.delete(id);
		
		return "redirect:/pacientes";
	}
	
	@RequestMapping("/editapaciente/{paciente}")
	public ModelAndView editar(@PathVariable Paciente paciente) {
		
		ModelAndView mv = new ModelAndView("pacienteEditar");
		
		mv.addObject(paciente);
		
		return mv;
	}
	
	@RequestMapping("/alterapaciente")
	public String editaralterar(@Validated Paciente paciente) {
		
		Paciente pacientenovo = paciente;

		this.pacientes.delete(paciente.getId());

		this.pacientes.save(pacientenovo);

		return "redirect:/pacientes";
	}

}
