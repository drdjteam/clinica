package br.com.clinica.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.clinica.repository.AlunoRepository;
import br.com.curso.service.AlunoService;

@PropertySource("classpath:application.properties") 
@Controller
public class AlunoImportController {
	
	@Autowired
	private AlunoRepository alunos;
	
    @Autowired
    AlunoService alunoService; //Service which will do all data retrieval/manipulation work
	
	@Value("${app.path.default}")
	private String defaultPath;
	
	@GetMapping("/alunoUpload")
	public void importAluno(String fileName) {
		System.out.println("upload page................");
	}

    @PostMapping("/handleFileUpload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
            RedirectAttributes redirectAttributes) throws IOException {
    	
    		String fileName = defaultPath +"/" + file.getOriginalFilename();

		// Get the file and save it somewhere
        byte[] bytes = file.getBytes();
        Path path = Paths.get(fileName);
        Files.write(path, bytes);
		
	    
		alunoService.importAlunoFromExcel(fileName);
		
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");

        return "redirect:/";
    }
	
	
}
