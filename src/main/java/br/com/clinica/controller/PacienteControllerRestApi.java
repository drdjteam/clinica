package br.com.clinica.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.clinica.model.Paciente;
import br.com.clinica.service.PacienteService;
import br.com.clinica.util.CustomErrorType;

/**
 * Created by danieldantas on 13/07/17.
 */
@RestController
@RequestMapping("/api")
public class PacienteControllerRestApi {
    public static final Logger logger = LoggerFactory.getLogger(PacienteControllerRestApi.class);

    @Autowired
    PacienteService pacienteService; //Service which will do all data retrieval/manipulation work

    // -------------------Retrieve All Pacientes---------------------------------------------
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/paciente/", method = RequestMethod.GET)
    public ResponseEntity<List<Paciente>> listAllPacientes() {
        List<Paciente> pacientes = pacienteService.findAllPacientes();
        if (pacientes.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Paciente>>(pacientes, HttpStatus.OK);
    }

    // -------------------Retrieve Single Paciente------------------------------------------
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/paciente/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getPaciente(@PathVariable("id") long id) {
        logger.info("Fetching Paciente with id {}", id);
        Paciente paciente = pacienteService.findById(id);
        if (paciente == null) {
            logger.error("Paciente with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType("Paciente with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
    }

    // -------------------Create a Paciente-------------------------------------------
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/paciente/", method = RequestMethod.POST)
    public ResponseEntity<?> createPaciente(@RequestBody Paciente paciente, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Paciente : {}", paciente);

        if (pacienteService.isPacienteExist(paciente)) {
            logger.error("Unable to create. A Paciente with name {} already exist", paciente.getNome());
            return new ResponseEntity(new CustomErrorType("Unable to create. A Paciente with name " +
                    paciente.getNome() + " already exist."),HttpStatus.CONFLICT);
        }
        pacienteService.savePaciente(paciente);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/paciente/{id}").buildAndExpand(paciente.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    // ------------------- Update a Paciente ------------------------------------------------
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/paciente/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updatePaciente(@PathVariable("id") long id, @RequestBody Paciente paciente) {
        logger.info("Updating Paciente with id {}", id);

        Paciente currentPaciente = pacienteService.findById(id);

        if (currentPaciente == null) {
            logger.error("Unable to update. Paciente with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType("Unable to upate. Paciente with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        currentPaciente.setNome(paciente.getNome());
        currentPaciente.setDocumento(paciente.getDocumento());
        currentPaciente.setEmail(paciente.getEmail());

        pacienteService.updatePaciente(currentPaciente);
        return new ResponseEntity<Paciente>(currentPaciente, HttpStatus.OK);
    }

    // ------------------- Delete a Paciente-----------------------------------------
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/paciente/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePaciente(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting Paciente with id {}", id);

        Paciente paciente = pacienteService.findById(id);
        if (paciente == null) {
            logger.error("Unable to delete. Paciente with id {} not found.", id);
            return new ResponseEntity(new CustomErrorType("Unable to delete. Paciente with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        pacienteService.deletePacienteById(id);
        return new ResponseEntity<Paciente>(HttpStatus.NO_CONTENT);
    }


}
