package br.com.clinica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.clinica.model.Ministrante;
import br.com.clinica.repository.MinistranteRepository;


@Controller
public class MinistranteController {
	
	@Autowired
	private MinistranteRepository ministrantes;
	
	@RequestMapping("/ministrantes")
	public ModelAndView listar() {
		
		//List<Ministrante> todosMinistrantes = ministrantes.findAll();
		List<br.com.clinica.model.Ministrante> todosMinistrantes = (List<Ministrante>) ministrantes.findAll();
		
		ModelAndView mv = new ModelAndView("ministranteListar");
		
		mv.addObject("ministrantes", todosMinistrantes);
		
		mv.addObject(new Ministrante());
		
		return mv;
	}
	
	@RequestMapping(value = "/ministrantes", method = RequestMethod.POST)
	public String salvar(@Validated Ministrante ministrante, Errors errors) {
		
		if ( errors.hasErrors() ) {
			return "redirect:/ministrantes";
		}
		
		this.ministrantes.save(ministrante);
		
		return "redirect:/ministrantes";
	}
	
	@RequestMapping("/excluiministrante/{id}")
	public String excluir(@PathVariable Long id) {
		
		this.ministrantes.delete(id);
		
		return "redirect:/ministrantes";
	}
	
	@RequestMapping("/editaministrante/{ministrante}")
	public ModelAndView editar(@PathVariable Ministrante ministrante) {
		
		ModelAndView mv = new ModelAndView("ministranteEditar");
		
		mv.addObject(ministrante);
		
		return mv;
	}
	
	@RequestMapping("/alteraministrante")
	public String editaralterar(@Validated Ministrante ministrante) {
		
		Ministrante ministrantenovo = ministrante;

		this.ministrantes.delete(ministrante.getId());

		this.ministrantes.save(ministrantenovo);

		return "redirect:/ministrantes";
	}

}
