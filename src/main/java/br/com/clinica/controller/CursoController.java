package br.com.clinica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.clinica.model.Curso;
import br.com.clinica.repository.CursoRepository;


@Controller
public class CursoController {
	
	@Autowired
	private CursoRepository cursos;
	
	@RequestMapping("/cursos")
	public ModelAndView listar() {
		
		//List<Curso> todosCursos = cursos.findAll();
		List<br.com.clinica.model.Curso> todosCursos = (List<Curso>) cursos.findAll();
		
		ModelAndView mv = new ModelAndView("cursoListar");
		
		mv.addObject("cursos", todosCursos);
		
		mv.addObject(new Curso());
		
		return mv;
	}
	
	@RequestMapping(value = "/cursos", method = RequestMethod.POST)
	public String salvar(@Validated Curso curso, Errors errors) {
		
		if ( errors.hasErrors() ) {
			return "redirect:/cursos";
		}
		
		this.cursos.save(curso);
		
		return "redirect:/cursos";
	}
	
	@RequestMapping("/excluicurso/{id}")
	public String excluir(@PathVariable Long id) {
		
		this.cursos.delete(id);
		
		return "redirect:/cursos";
	}
	
	@RequestMapping("/editacurso/{curso}")
	public ModelAndView editar(@PathVariable Curso curso) {
		
		ModelAndView mv = new ModelAndView("cursoEditar");
		
		mv.addObject(curso);
		
		return mv;
	}
	
	@RequestMapping("/alteracurso")
	public String editaralterar(@Validated Curso curso) {
		
		Curso cursonovo = curso;

		this.cursos.delete(curso.getId());

		this.cursos.save(cursonovo);

		return "redirect:/cursos";
	}

}
