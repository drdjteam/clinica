package br.com.clinica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.clinica.model.Profissional;
import br.com.clinica.repository.ProfissionalRepository;

@Controller
public class ProfissionalController {
	
	@Autowired
	private ProfissionalRepository profissionais;
	
	@RequestMapping("/profissionais")
	public ModelAndView listar() {
		
		//List<Profissional> todosProfissionais = profissionais.findAll();
		List<Profissional> todosProfissionais = (List<Profissional>) profissionais.findAll();
		
		ModelAndView mv = new ModelAndView("profissionalListar");
		
		mv.addObject("profissionais", todosProfissionais);
		
		mv.addObject(new Profissional());
		
		return mv;
	}
	
	@RequestMapping(value = "/profissionais", method = RequestMethod.POST)
	public String salvar(@Validated Profissional profissional, Errors errors) {
		
		if ( errors.hasErrors() ) {
			return "redirect:/profissionais";
		}
		
		this.profissionais.save(profissional);
		
		return "redirect:/profissionais";
	}
	
	@RequestMapping("/excluiprofissional/{id}")
	public String excluir(@PathVariable Long id) {
		
		this.profissionais.delete(id);
		
		return "redirect:/profissionais";
	}
	
	@RequestMapping("/editaprofissional/{profissional}")
	public ModelAndView editar(@PathVariable Profissional profissional) {
		
		ModelAndView mv = new ModelAndView("profissionalEditar");
		
		mv.addObject(profissional);
		
		return mv;
	}
	
	@RequestMapping("/alteraprofissional")
	public String editaralterar(@Validated Profissional profissional) {
		
		Profissional profissionalnovo = profissional;

		this.profissionais.delete(profissional.getId());

		this.profissionais.save(profissionalnovo);

		return "redirect:/profissionais";
	}

}
