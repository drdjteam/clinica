package br.com.util;


import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import br.com.clinica.model.Aluno;


public class ExcelImport {

	private static final String FILE_NAME = "/Users/danieldantas/Dropbox/Projetos/Ludens/PlanilhasImportacao/PlanilhasOficiaisParaImportaçãoALUNO/Planilhas JF FLUXE/Arquivo contato Ludens_Dados alunos e interessados (1).xlsx";
	private static final String SHEET_TYPE = "ALUNO2";
    
    public void readColumnsBySequence() {
    		
    				
    		XSSFWorkbook wb; 
		XSSFSheet sheet;
		XSSFRow row; 
		XSSFCell cell;
	
    		try {
			InputStream ExcelFileToRead = new FileInputStream(FILE_NAME);
			wb = new XSSFWorkbook(ExcelFileToRead);
			sheet = wb.getSheetAt(0);
	
			Iterator<?> rows = sheet.rowIterator();
	
			while (rows.hasNext())
			{
				
				row=(XSSFRow) rows.next();
				Iterator<?> cells = row.cellIterator();

				if (SHEET_TYPE == "ALUNO") {
					
				}else{

					while (cells.hasNext())
					{
						cell=(XSSFCell) cells.next();
						
						if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
						{
							System.out.print(cell.getStringCellValue()+" ");
						}
						else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
						{
							System.out.print(cell.getNumericCellValue()+" ");
						}
						else
						{
							//U Can Handel Boolean, Formula, Errors
						}
					}
					System.out.println();
				
				}			
			}
    		}catch(Exception e) {
    			e.printStackTrace();
    		}
	}
    
    
    public static List<Aluno> importAluno(String fileName) {
		XSSFWorkbook wb; 
		XSSFSheet sheet;
		XSSFRow row; 
		XSSFCell cell;

		List<String> columnsTitle = new ArrayList<String>();
		List<Aluno> alunoList = new ArrayList<Aluno>();
		String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
		
		try {
			InputStream ExcelFileToRead = new FileInputStream(fileName);
			wb = new XSSFWorkbook(ExcelFileToRead);
			sheet = wb.getSheetAt(0);
			Iterator<?> rows = sheet.rowIterator();
	
			while (rows.hasNext()){
				row=(XSSFRow) rows.next();
				
				if (row.getRowNum() == 0) { // Columns TITLE
					
					Iterator<?> cells = row.cellIterator();
					
					//Read columns sequentially and store every title in array
					while (cells.hasNext()) {
						cell=(XSSFCell) cells.next();
						
						columnsTitle.add(cell.getStringCellValue());
					}
					
					for (Iterator iterator = columnsTitle.iterator(); iterator.hasNext();) {
						String title = (String) iterator.next();
					}
					
				}else {
					
					DataFormatter df = new DataFormatter();
					Aluno aluno = new Aluno();
					XSSFCell xssfCell;
					
					xssfCell = row.getCell(0);
					if (xssfCell != null) {
						Date dataInscricao =  DateTimeUtil.convertStringToDate(df.formatCellValue(xssfCell), dateTimeFormat); 
						aluno.setDataInscricao(dataInscricao);
					}
					
					String nome = df.formatCellValue(row.getCell(1));
					aluno.setNome(nome);
					
					xssfCell = row.getCell(2);
					if (xssfCell != null) {
						String nacionalidade = df.formatCellValue(xssfCell);
						aluno.setNacionalidade(nacionalidade);
					}
					
					xssfCell = row.getCell(3);
					if (xssfCell != null) {
						String estadoCivil = df.formatCellValue(xssfCell);
						aluno.setEstadoCivil(estadoCivil);
					}
					
					xssfCell = row.getCell(4);
					if (xssfCell != null) {
						String rg = df.formatCellValue(xssfCell);
						aluno.setRg(rg);
					}
					
					xssfCell = row.getCell(5);
					if (xssfCell != null) {
						String rgOrgaoEmissorUf = df.formatCellValue(xssfCell);
						aluno.setRgOrgaoEmissorUF(rgOrgaoEmissorUf);
					}	
					
					xssfCell = row.getCell(6);
					if (xssfCell != null) {
						String cpf = df.formatCellValue(xssfCell);
						aluno.setCpf(cpf);
					}
					
					xssfCell = row.getCell(7);
					if (xssfCell != null) {
						String profissao = xssfCell.getStringCellValue();
						aluno.setProfissao(profissao);
					}
					
					xssfCell = row.getCell(8);
					if (xssfCell != null) {
						String endereco = xssfCell.getStringCellValue();
						aluno.setEndereco(endereco);
					}
					
					xssfCell = row.getCell(9);
					if (xssfCell != null) {
						Integer numero;
						try {
							numero= (int) xssfCell.getNumericCellValue();
						}catch(Exception e) {
							numero=0;
						}
						aluno.setNumero(numero);
					}
					
					xssfCell = row.getCell(10);
					if (xssfCell != null) {
						String complemento = df.formatCellValue(xssfCell);
						aluno.setComplemento(complemento);
					}

					xssfCell = row.getCell(11);
					if (xssfCell != null) {
						String bairro = xssfCell.getStringCellValue();
						aluno.setBairro(bairro);
					}

					xssfCell = row.getCell(12);
					if (xssfCell != null) {
						String cidade = xssfCell.getStringCellValue();
						aluno.setCidade(cidade);
					}
					
					xssfCell = row.getCell(13);
					if (xssfCell != null) {
						String estado = xssfCell.getStringCellValue();
						aluno.setEstado(estado);
					}
					
					xssfCell = row.getCell(14);
					if (xssfCell != null) {
						String cep = df.formatCellValue(xssfCell);
						aluno.setCep(cep);
					}
					
					xssfCell = row.getCell(15);
					if (xssfCell != null) {
						String telefoneResidencial = df.formatCellValue(xssfCell);
						aluno.setTelefoneResidencial(telefoneResidencial);
					}
					
					xssfCell = row.getCell(16);
					if (xssfCell != null) {
						String telefoneComercial = df.formatCellValue(xssfCell);
						aluno.setTelefoneComercial(telefoneComercial);
					}
					
					xssfCell = row.getCell(17);
					if (xssfCell != null) {
						String telefoneCelular = df.formatCellValue(xssfCell);
						aluno.setTelefoneCelular(telefoneCelular);
					}
					
					xssfCell = row.getCell(18);
					if (xssfCell != null) {
						String email = df.formatCellValue(xssfCell);
						aluno.setEmail(email);
					}
					
					xssfCell = row.getCell(19);
					if (xssfCell != null) {
						String contatoEmergenciaNome = df.formatCellValue(xssfCell);
						aluno.setContatoEmergenciaNome(contatoEmergenciaNome);
					}
					
					xssfCell = row.getCell(20);
					if (xssfCell != null) {
						String contatoEmergenciaTelefone = df.formatCellValue(xssfCell);
						aluno.setContatoEmergenciaTelefone(contatoEmergenciaTelefone);
					}
					
					xssfCell = row.getCell(21);
					if (xssfCell != null) {
						String comoSoubeCurso = df.formatCellValue(xssfCell);
						aluno.setComoSoubeCurso(comoSoubeCurso);
					}
					
					xssfCell = row.getCell(22);
					if (xssfCell != null) {
						Boolean exAluno = (df.formatCellValue(xssfCell).toUpperCase().substring(0, 1).equals("S")) ? true : false;
						aluno.setExAluno(exAluno);
					}
					
					xssfCell = row.getCell(23);
					if (xssfCell != null) {
						String exAlunoNomeCurso = df.formatCellValue(xssfCell);
						aluno.setExAlunoNomeCurso(exAlunoNomeCurso);
					}
					
					try {
						xssfCell = row.getCell(24);
						if (xssfCell != null) {
							Integer exAlunoAnoRealizacao;
							try {
								exAlunoAnoRealizacao = (int) xssfCell.getNumericCellValue();
							}catch(Exception e) {
								exAlunoAnoRealizacao = 0;
							}
							
							aluno.setExAlunoAnoRealizacao(exAlunoAnoRealizacao);
						}
					}catch(Exception exx) {
						exx.printStackTrace();
					}
					
					xssfCell = row.getCell(25);
					if (xssfCell != null) {
						String observacaoPagamento = df.formatCellValue(xssfCell);
						aluno.setObservacaoPagamento(observacaoPagamento);
					}
					
					xssfCell = row.getCell(26);
					if (xssfCell != null) {
						String observacaoPagamentoExAluno = df.formatCellValue(xssfCell);
						aluno.setObservacaoPagamentoExAluno(observacaoPagamentoExAluno);
					}
					
					aluno.setFileNameImport(fileName);
					
					alunoList.add(aluno);
				}
	
	//			row.getCell(cellnum)
	//			Iterator<?> cells = row.cellIterator();	
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return alunoList;
    }
    
    public static void main(String[] args) {
    		ExcelImport.importAluno("/Users/danieldantas/Dropbox/Projetos/Ludens/PlanilhasImportacao/PlanilhasOficiaisParaImportaçãoALUNO/Planilhas JF FLUXE/Arquivo contato Ludens_Dados alunos e interessados (1).xlsx");
    }
}

