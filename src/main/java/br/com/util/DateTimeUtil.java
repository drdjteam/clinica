package br.com.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateTimeUtil {
	
	/**
	 * @param dateString
	 * @param dateTimeFormat
	 * @return Date
	 * ex: dateString= "25/01/2017  07:49:32" , dateTimeFormat = "dd/MM/yyyy  HH:mm:ss"
	 */
	public static Date convertStringToDate(String dateString, String dateTimeFormat) {
		
		ZoneId defaultZoneId = ZoneId.systemDefault();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);
	
	    LocalDateTime localDateTime = LocalDateTime.parse(dateString, formatter);
	    
	    Date dateResult = Date.from(localDateTime.atZone(defaultZoneId).toInstant());
	    
//	    System.out.println(dateResult);
//	    System.out.println(localDateTime);
	    
	   return dateResult;
	}
	
	public static void main(String[] args) {
		String dateTimeFormat = "dd/MM/yyyy  HH:mm:ss";
		String dataInscricao = "25/01/2017  07:49:32";

		convertStringToDate(dataInscricao, dateTimeFormat);
	}
}	
