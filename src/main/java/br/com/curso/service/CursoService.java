package br.com.curso.service;

import java.util.List;

import br.com.clinica.model.Curso;

public interface CursoService {
    public Curso findById(Long id);

    public List<Curso> findByTitulo(String titulo);

    public void saveCurso(Curso curso);

    public void updateCurso(Curso curso);

    public void deleteCursoById(Long id);

    public List<Curso> findAllCursos();

    public boolean isCursoExist(Curso curso);
}
