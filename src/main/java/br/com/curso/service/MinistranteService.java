package br.com.curso.service;

import java.util.List;

import br.com.clinica.model.Ministrante;

public interface MinistranteService {
    public Ministrante findById(Long id);

    public List<Ministrante> findByNome(String nome);

    public void saveMinistrante(br.com.clinica.model.Ministrante ministrante);

    public void updateMinistrante(Ministrante ministrante);

    public void deleteMinistranteById(Long id);

    public List<Ministrante> findAllMinistrantes();

    public boolean isMinistranteExist(Ministrante ministrante);
}
