package br.com.curso.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.clinica.model.Ministrante;
import br.com.clinica.repository.MinistranteRepository;

@Service("ministranteService")
@Transactional
public class MinistranteServiceImpl implements MinistranteService{

    @Autowired
    private MinistranteRepository repository;

    @Override
    public Ministrante findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public List<Ministrante> findByNome(String nome) {
        return repository.findByNome(nome);
    }

    @Override
    public void saveMinistrante(Ministrante ministrante) {
        repository.save(ministrante);
    }

    @Override
    public void updateMinistrante(Ministrante ministrante) {
        saveMinistrante(ministrante);
    }

    @Override
    public void deleteMinistranteById(Long id) {
        repository.delete(id);
    }

    @Override
    public List<Ministrante> findAllMinistrantes() {
        return repository.findAll();
    }

    @Override
    public boolean isMinistranteExist(Ministrante ministrante) {
        boolean ret = false;
        List<Ministrante> ministranteList = repository.findByNome(ministrante.getNome());
        if (ministranteList != null) {
            if (ministranteList.size() > 0) {
                ret = true;
            }
        }

        return ret;
    }
}
