package br.com.curso.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.clinica.controller.PacienteControllerRestApi;
import br.com.clinica.model.Aluno;
import br.com.clinica.repository.AlunoRepository;
import br.com.util.ExcelImport;

@Service("alunoService")
@Transactional
public class AlunoServiceImpl implements AlunoService{
    public static final Logger logger = LoggerFactory.getLogger(AlunoServiceImpl.class);

    @Autowired
    private AlunoRepository repository;

    @Override
    public Aluno findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public List<Aluno> findByNome(String nome) {
        return repository.findByNome(nome);
    }

    @Override
    public void saveAluno(Aluno aluno) {
        repository.save(aluno);
    }

    @Override
    public void updateAluno(Aluno aluno) {
        saveAluno(aluno);
    }

    @Override
    public void deleteAlunoById(Long id) {
        repository.delete(id);
    }

    @Override
    public List<Aluno> findAllAlunos() {
        return repository.findAll();
    }

    @Override
    public boolean isAlunoExist(Aluno aluno) {
        boolean ret = false;
        List<Aluno> alunoList = repository.findByNome(aluno.getNome());
        if (alunoList != null) {
            if (alunoList.size() > 0) {
                ret = true;
            }
        }

        return ret;
    }

	@Override
	public void importAlunoFromExcel(String fileName) {
		List<Aluno> alunoList;
		alunoList = ExcelImport.importAluno(fileName); 
		
		for (Aluno aluno : alunoList) {
			//só add se o nome não existir
			if (this.findByNome(aluno.getNome()) == null || this.findByNome(aluno.getNome()).size() == 0) {
				this.saveAluno(aluno);
			}else {
				logger.warn("Importação - Aluno duplicado: " + fileName + " - " + aluno.getNome());
			}
		
		}
		
	}
}
