package br.com.curso.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.clinica.model.Curso;
import br.com.clinica.repository.CursoRepository;

@Service("cursoService")
@Transactional
public class CursoServiceImpl implements CursoService{

    @Autowired
    private CursoRepository repository;

    @Override
    public Curso findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public List<Curso> findByTitulo(String titulo) {
        return repository.findByTitulo(titulo);
    }

    @Override
    public void saveCurso(Curso curso) {
        repository.save(curso);
    }

    @Override
    public void updateCurso(Curso curso) {
        saveCurso(curso);
    }

    @Override
    public void deleteCursoById(Long id) {
        repository.delete(id);
    }

    @Override
    public List<Curso> findAllCursos() {
        return repository.findAll();
    }

    @Override
    public boolean isCursoExist(Curso curso) {
        boolean ret = false;
        List<Curso> cursoList = repository.findByTitulo(curso.getTitulo());
        if (cursoList != null) {
            if (cursoList.size() > 0) {
                ret = true;
            }
        }

        return ret;
    }
}
