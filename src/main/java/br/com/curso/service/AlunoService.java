package br.com.curso.service;

import java.util.List;

import br.com.clinica.model.Aluno;

public interface AlunoService {
    public Aluno findById(Long id);

    public List<Aluno> findByNome(String nome);

    public void saveAluno(Aluno aluno);

    public void updateAluno(Aluno aluno);

    public void deleteAlunoById(Long id);

    public List<Aluno> findAllAlunos();

    public boolean isAlunoExist(Aluno aluno);
    
    public void importAlunoFromExcel(String fileName);
}
