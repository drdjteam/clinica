/**
 * Created by danieldantas on 13/07/17.
 */

'use strict';

angular.module('crudApp').factory('PacienteService',
    ['$localStorage', '$http', '$q', 'urls',
        function ($localStorage, $http, $q, urls) {

            var factory = {
                loadAllPacientes: loadAllPacientes,
                getAllPacientes: getAllPacientes,
                getPaciente: getPaciente,
                createPaciente: createPaciente,
                updatePaciente: updatePaciente,
                removePaciente: removePaciente
            };

            return factory;

            function loadAllPacientes() {
                console.log('Fetching all pacientes');
                var deferred = $q.defer();
                $http.get(urls.PACIENTE_SERVICE_API)
                    .then(
                        function (response) {
                            console.log('Fetched successfully all pacientes');
                            $localStorage.pacientes = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading pacientes');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function getAllPacientes(){
                return $localStorage.pacientes;
            }

            function getPaciente(id) {
                console.log('Fetching Paciente with id :'+id);
                var deferred = $q.defer();
                $http.get(urls.PACIENTE_SERVICE_API + id)
                    .then(
                        function (response) {
                            console.log('Fetched successfully Paciente with id :'+id);
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while loading paciente with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function createPaciente(paciente) {
                console.log('Creating Paciente');
                var deferred = $q.defer();
                $http.post(urls.PACIENTE_SERVICE_API, paciente)
                    .then(
                        function (response) {
                            loadAllPacientes();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while creating Paciente : '+errResponse.data.errorMessage);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function updatePaciente(paciente, id) {
                console.log('Updating Paciente with id '+id);
                var deferred = $q.defer();
                $http.put(urls.PACIENTE_SERVICE_API + id, paciente)
                    .then(
                        function (response) {
                            loadAllPacientes();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while updating Paciente with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function removePaciente(id) {
                console.log('Removing Paciente with id '+id);
                var deferred = $q.defer();
                $http.delete(urls.PACIENTE_SERVICE_API + id)
                    .then(
                        function (response) {
                            loadAllPacientes();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while removing Paciente with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

        }
    ]);