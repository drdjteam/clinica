/**
 * Created by danieldantas on 13/07/17.
 */
'use strict';

angular.module('crudApp').controller('PacienteController',
    ['PacienteService', '$scope',  function( PacienteService, $scope) {

        var self = this;
        self.paciente = {};
        self.pacientes=[];

        self.submit = submit;
        self.getAllPacientes = getAllPacientes;
        self.createPaciente = createPaciente;
        self.updatePaciente = updatePaciente;
        self.removePaciente = removePaciente;
        self.editPaciente = editPaciente;
        self.reset = reset;

        self.successMessage = '';
        self.errorMessage = '';
        self.done = false;

        self.onlyIntegers = /^\d+$/;
        self.onlyNumbers = /^\d+([,.]\d+)?$/;

        function submit() {
            console.log('Submitting');
            if (self.paciente.id === undefined || self.paciente.id === null) {
                console.log('Saving New Paciente', self.paciente);
                createPaciente(self.paciente);
            } else {
                updatePaciente(self.paciente, self.paciente.id);
                console.log('Paciente updated with id ', self.paciente.id);
            }
        }

        function createPaciente(paciente) {
            console.log('About to create paciente');
            PacienteService.createPaciente(paciente)
                .then(
                    function (response) {
                        console.log('Paciente adicionado com sucesso!');
                        self.successMessage = 'Paciente adicionado com sucesso!';
                        self.errorMessage='';
                        self.done = true;
                        self.paciente={};
                        $scope.myForm.$setPristine();
                    },
                    function (errResponse) {
                        console.error('Ocorreu um erro ao adicionar um Paciente');
                        self.errorMessage = 'Ocorreu um erro ao adicionar o Paciente: ' + errResponse.data.errorMessage;
                        self.successMessage='';
                    }
                );
        }


        function updatePaciente(paciente, id){
            console.log('About to update paciente');
            PacienteService.updatePaciente(paciente, id)
                .then(
                    function (response){
                        console.log('Paciente updated successfully');
                        self.successMessage = 'Paciente atualizado com sucesso';
                        self.errorMessage='';
                        self.done = true;
                        $scope.myForm.$setPristine();
                    },
                    function(errResponse){
                        console.error('Error while updating Paciente');
                        self.errorMessage = 'Não foi possível atualizar o Paciente ' + errResponse.data;
                        self.successMessage='';
                    }
                );
        }


        function removePaciente(id){
            console.log('About to remove Paciente with id '+id);
            PacienteService.removePaciente(id)
                .then(
                    function(){
                        console.log('Paciente ' + id + ' removido com sucesso!');
                    },
                    function(errResponse){
                        console.error('Error while removing paciente '+id +', Error :'+errResponse.data);
                    }
                );
        }


        function getAllPacientes(){
            return PacienteService.getAllPacientes();
        }

        function editPaciente(id) {
            self.successMessage='';
            self.errorMessage='';
            PacienteService.getPaciente(id).then(
                function (paciente) {
                    self.paciente = paciente;
                },
                function (errResponse) {
                    console.error('Error while removing paciente ' + id + ', Error :' + errResponse.data);
                }
            );
        }
        function reset(){
            self.successMessage='';
            self.errorMessage='';
            self.paciente={};
            $scope.myForm.$setPristine(); //reset Form
        }
    }
    ]);