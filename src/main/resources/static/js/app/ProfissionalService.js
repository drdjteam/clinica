/**
 * Created by danieldantas on 13/07/17.
 */

'use strict';

angular.module('crudApp').factory('ProfissionalService',
    ['$localStorage', '$http', '$q', 'urls',
        function ($localStorage, $http, $q, urls) {

            var factory = {
                loadAllProfissionals: loadAllProfissionals,
                getAllProfissionals: getAllProfissionals,
                getProfissional: getProfissional,
                createProfissional: createProfissional,
                updateProfissional: updateProfissional,
                removeProfissional: removeProfissional
            };

            return factory;

            function loadAllProfissionals() {
                console.log('Fetching all pacientes');
                var deferred = $q.defer();
                $http.get(urls.PROFISSIONAL_SERVICE_API)
                    .then(
                        function (response) {
                            console.log('Fetched successfully all pacientes');
                            $localStorage.pacientes = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading pacientes');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function getAllProfissionals(){
                return $localStorage.pacientes;
            }

            function getProfissional(id) {
                console.log('Fetching Profissional with id :'+id);
                var deferred = $q.defer();
                $http.get(urls.PROFISSIONAL_SERVICE_API + id)
                    .then(
                        function (response) {
                            console.log('Fetched successfully Profissional with id :'+id);
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while loading paciente with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function createProfissional(paciente) {
                console.log('Creating Profissional');
                var deferred = $q.defer();
                $http.post(urls.PROFISSIONAL_SERVICE_API, paciente)
                    .then(
                        function (response) {
                            loadAllProfissionals();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while creating Profissional : '+errResponse.data.errorMessage);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function updateProfissional(paciente, id) {
                console.log('Updating Profissional with id '+id);
                var deferred = $q.defer();
                $http.put(urls.PROFISSIONAL_SERVICE_API + id, paciente)
                    .then(
                        function (response) {
                            loadAllProfissionals();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while updating Profissional with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function removeProfissional(id) {
                console.log('Removing Profissional with id '+id);
                var deferred = $q.defer();
                $http.delete(urls.PROFISSIONAL_SERVICE_API + id)
                    .then(
                        function (response) {
                            loadAllProfissionals();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while removing Profissional with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

        }
    ]);