/**
 * Created by danieldantas on 13/07/17.
 */
'use strict';

angular.module('crudApp').controller('ProfissionalController',
    ['ProfissionalService', '$scope',  function( ProfissionalService, $scope) {

        var self = this;
        self.profissional = {};
        self.profissionals=[];

        self.submit = submit;
        self.getAllProfissionals = getAllProfissionals;
        self.createProfissional = createProfissional;
        self.updateProfissional = updateProfissional;
        self.removeProfissional = removeProfissional;
        self.editProfissional = editProfissional;
        self.reset = reset;

        self.successMessage = '';
        self.errorMessage = '';
        self.done = false;

        self.onlyIntegers = /^\d+$/;
        self.onlyNumbers = /^\d+([,.]\d+)?$/;

        function submit() {
            console.log('Submitting');
            if (self.profissional.id === undefined || self.profissional.id === null) {
                console.log('Saving New Profissional', self.profissional);
                createProfissional(self.profissional);
            } else {
                updateProfissional(self.profissional, self.profissional.id);
                console.log('Profissional updated with id ', self.profissional.id);
            }
        }

        function createProfissional(profissional) {
            console.log('About to create profissional');
            ProfissionalService.createProfissional(profissional)
                .then(
                    function (response) {
                        console.log('Profissional adicionado com sucesso!');
                        self.successMessage = 'Profissional adicionado com sucesso!';
                        self.errorMessage='';
                        self.done = true;
                        self.profissional={};
                        $scope.myForm.$setPristine();
                    },
                    function (errResponse) {
                        console.error('Ocorreu um erro ao adicionar um Profissional');
                        self.errorMessage = 'Ocorreu um erro ao adicionar o Profissional: ' + errResponse.data.errorMessage;
                        self.successMessage='';
                    }
                );
        }


        function updateProfissional(profissional, id){
            console.log('About to update profissional');
            ProfissionalService.updateProfissional(profissional, id)
                .then(
                    function (response){
                        console.log('Profissional updated successfully');
                        self.successMessage = 'Profissional atualizado com sucesso';
                        self.errorMessage='';
                        self.done = true;
                        $scope.myForm.$setPristine();
                    },
                    function(errResponse){
                        console.error('Error while updating Profissional');
                        self.errorMessage = 'Não foi possível atualizar o Profissional ' + errResponse.data;
                        self.successMessage='';
                    }
                );
        }


        function removeProfissional(id){
            console.log('About to remove Profissional with id '+id);
            ProfissionalService.removeProfissional(id)
                .then(
                    function(){
                        console.log('Profissional ' + id + ' removido com sucesso!');
                    },
                    function(errResponse){
                        console.error('Error while removing profissional '+id +', Error :'+errResponse.data);
                    }
                );
        }


        function getAllProfissionals(){
            return ProfissionalService.getAllProfissionals();
        }

        function editProfissional(id) {
            self.successMessage='';
            self.errorMessage='';
            ProfissionalService.getProfissional(id).then(
                function (profissional) {
                    self.profissional = profissional;
                },
                function (errResponse) {
                    console.error('Error while removing profissional ' + id + ', Error :' + errResponse.data);
                }
            );
        }
        function reset(){
            self.successMessage='';
            self.errorMessage='';
            self.profissional={};
            $scope.myForm.$setPristine(); //reset Form
        }
    }
    ]);