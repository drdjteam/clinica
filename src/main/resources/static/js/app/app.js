/**
 * Created by danieldantas on 13/07/17.
 */
var app = angular.module('crudApp',['ui.router','ngStorage']);

app.constant('urls', {
    BASE: 'http://localhost:8080/clinica',
    USER_SERVICE_API : 'http://localhost:8080/clinica/api/paciente/',
    PACIENTE_SERVICE_API : 'http://localhost:8080/clinica/api/paciente/',
    PROFISSIONAL_SERVICE_API : 'http://localhost:8080/clinica/api/profissional/'
});

app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'partials/paciente',
                controller:'PacienteController',
                controllerAs:'ctrl',
                resolve: {
                    pacientes: function ($q, PacienteService) {
                        console.log('Load all pacientes');
                        var deferred = $q.defer();
                        PacienteService.loadAllPacientes().then(deferred.resolve, deferred.resolve);
                        return deferred.promise;
                    }
                }
            })

            .state('profissional', {
                url: '/profissional',
                templateUrl: 'partials/profissional',
                controller:'ProfissionalController',
                controllerAs:'ctrl',
                resolve: {
                    profissionals: function ($q, ProfissionalService) {
                        console.log('Load all profissionais');
                        var deferred = $q.defer();
                        PacienteService.loadAllPacientes().then(deferred.resolve, deferred.resolve);
                        return deferred.promise;
                    }
                }
            });
        $urlRouterProvider.otherwise('/');
    }]);